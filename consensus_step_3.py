import numpy as np
import csv
import os
import string
from utils import load_data
from utils import ensemble_clust
from utils import draw_similarity_matrix
import scipy.cluster.hierarchy as sch
import matplotlib.pyplot as plt
import pylab
from sklearn.cluster import spectral_clustering

flag_solution = "AllInput"
flag_dropsample = False

if flag_solution == 'AllInput':
    if flag_dropsample == True:
        dir_sol = "sol_consensus_AllInput_dropsample"
        dir_fig = "fig_consensus_AllInput_dropsample"
    else:
        dir_sol = "sol_consensus_AllInput"
        dir_fig = "fig_consensus_AllInput"
elif flag_solution == "CaseInput":
    if flag_dropsample == True:
        dir_sol = "sol_consensus_CaseInput_dropsample"
        dir_fig = "fig_consensus_CaseInput_dropsample"
    else:
        dir_sol = "sol_consensus_CaseInput"
        dir_fig = "fig_consensus_CaseInput"
else:
    pass


# load all solutions
solution,feature_of_interest,subtype_name,description = \
        load_data.load_solutions(dir_sol)

# whether to compute overlap between two clusters before computing concordance
# score
flag_overlap = True

# compute the total number of cluster and index them
solution_id = solution.keys()
n_solution = len(solution_id)

cluster_id_pre = []
cluster_id_suf = []
for item in solution_id:
    tmp = list(set(solution[item].values()).difference({-9}))
    for i in tmp:
        cluster_id_pre.append(item)
        cluster_id_suf.append(i)

# total number of clusters
n_clusters = len(cluster_id_pre)

# compute similarity between clusters
sim_clust = np.zeros((n_clusters,n_clusters))

# find size of each cluster
size_clust = np.zeros(n_clusters)

# find the overlap between samples of two solutions as base set for
# computing concordance between two clusters
for i in range(n_clusters):
    # find case_ids for samples in cluster i
    clust_i = []
    for case_id,label in solution[cluster_id_pre[i]].iteritems():
        if label == cluster_id_suf[i]:
            clust_i.append(case_id)
    size_clust[i] = len(clust_i)
    for j in range(i,n_clusters):
        clust_j = []
        for case_id,label in solution[cluster_id_pre[j]].iteritems():
            if label == cluster_id_suf[j]:
                clust_j.append(case_id)
        
        # find the overlap between samples of two solutions as base set for
        # computing concordance between two clusters
        if flag_overlap == True:
            tmp = set(solution[cluster_id_pre[i]].keys()).intersection(\
                    set(solution[cluster_id_pre[j]].keys()))
            tmp_deno = len(set(clust_i).union(set(clust_j)).intersection(tmp))
            
            if tmp_deno > 0:
                sim_clust[i,j] = len(set(clust_i).intersection(\
                        set(clust_j)).intersection(tmp))*1./tmp_deno
            else:
                sim_clust[i,j] = 0
        else:
            sim_clust[i,j] = len(set(clust_i).intersection(set(clust_j)))*\
                    1./len(set(clust_i).union(set(clust_j)))
        
        sim_clust[j,i] = sim_clust[i,j]

# find the number of cluster groups
#for n_cluster in range(2,15):
for n_cluster in [8]:
    # apply spectral clustering
    label_pred = spectral_clustering(sim_clust,n_clusters=n_cluster)
    # draw similarity matrix
    pos_old = draw_similarity_matrix.draw_similarity_matrix(sim_clust,label_pred,\
            n_cluster,"consensus2/"+'sim_clust_K_'+str(n_cluster)+'.png')


cluster_id_pre_reorder = []
cluster_id_suf_reorder = []
cluster_id_reorder = []
for i in range(len(label_pred)):
    k = pos_old[i]
    cluster_id_pre_reorder.append(cluster_id_pre[k])
    cluster_id_suf_reorder.append(cluster_id_suf[k])
    cluster_id_reorder.append(cluster_id_pre[k] + '_' + \
            str(cluster_id_suf[k]))

cluster_id_group = {}

# print out for debug purpose
for i in range(len(label_pred)):
    #print label_pred[pos_old[i]],cluster_id_reorder[i]
    if label_pred[pos_old[i]] not in cluster_id_group.keys():
        cluster_id_group[label_pred[pos_old[i]]] = [cluster_id_reorder[i]]
    else:
        cluster_id_group[label_pred[pos_old[i]]].append(cluster_id_reorder[i])

pos_old_list = []
# find consensus for clusters in one group
for k in cluster_id_group.keys():
    # for each cluster, use cluster_id as key, value is {case_id,cluster
    # assignment} dictionary
    print k
    clusters = {}
    for cluster_id in cluster_id_group[k]:
        tmp = cluster_id.split('_')
        assert len(tmp) >= 2
        if len(tmp) == 3:
            sol_id = tmp[0]+'_'+tmp[1]
            clt_id = int(tmp[2])
        if len(tmp) == 2:
            sol_id = tmp[0]
            clt_id = int(tmp[1])

        tmp_dict = {}
        for item in solution[sol_id].keys():
            if solution[sol_id][item] == clt_id:
                tmp_dict[item] = clt_id
        clusters[cluster_id] = tmp_dict
    # build hypergraph
    H,case_id_unique = ensemble_clust.construct_hypergraph(clusters,True)
    S = 1./len(clusters) * H.dot(H.T)
    label_pred_clust = spectral_clustering(S,n_clusters=2)
    # draw similarity matrix
    if not os.path.exists(dir_fig):
        os.makedirs(dir_fig)
    pos_old = draw_similarity_matrix.draw_similarity_matrix(S,label_pred_clust,\
            2,dir_fig+"/sim_consensus_cluster_"+str(k)+'.png')
    pos_old_list.append(pos_old)

    # write consensus solution into a csv file
    if not os.path.exists(dir_sol+"_con"):
        os.makedirs(dir_sol+"_con")
    csvfile = open(dir_sol+"_con"+"/consensus_cluster_"+str(k),"wb")
    csvwriter = csv.writer(csvfile)
    assert len(label_pred_clust) == len(case_id_unique)
    for i in range(len(case_id_unique)):
        csvwriter.writerow([case_id_unique[i],label_pred_clust[i]])
    csvfile.close()

# write cluster_id_group in csv file
csvfile = open(dir_sol+"_con/" + "cluster_id_group.csv","wb")
csvwriter = csv.writer(csvfile)
for k in cluster_id_group.keys():
    csvwriter.writerow([k]+cluster_id_group[k])
csvfile.close()

