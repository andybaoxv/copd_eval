""" Since each investigator submit multiple solutions. Direct consensus will
introduce bias for preferring the investigator with more solutions. Therefore,
we first apply consensus clustering for solutions belonging to one
investigator's particular method. The details of grouping solutions is
specified in "subList_yale.csv"
The output of this script is consensus solutions stored in directory 
"AllInput_yale"
"""

import numpy as np
import csv
import os
import string
from utils import ensemble_clust
from utils import draw_similarity_matrix
from utils import load_data
import scipy.cluster.hierarchy as sch
import matplotlib.pyplot as plt
import pylab
from sklearn.cluster import spectral_clustering
import os

# load all solutions
csvfile = open("allSubmissions.csv","rb")
csvreader = csv.reader(csvfile)
lines = [line for line in csvreader]

# case id for all patients
case_id = []

# each solution is indexed by its name, its value is a list
solution = {}
for j in range(1,len(lines[0])):
    solution[lines[0][j]] = []

for k in range(1,len(lines)):
    case_id.append(lines[k][0])
    for j in range(1,len(lines[k])):
        solution[lines[0][j]].append(lines[k][j])
csvfile.close()

# debug purpose: check Mike's "allSubmissions" is consistent with what I get
"""
solution_dir,f_in,s_name,descr = load_data.load_solutions("AllInput")
case_id_dir = case_id
for k in solution_dir.keys():
    tmp = ['NA'] * len(case_id_dir)
    for i in solution_dir[k].keys():
        if i in case_id_dir:
            tmp[case_id_dir.index(i)] = str(solution_dir[k][i])
    solution_dir[k] = tmp

for k in solution.keys():
    assert solution[k] == solution_dir[k]

"""

# write solutions in "CaseInput" into a single csvfile "allSubmissions_case"
"""
solution_case,f_case,s_case,desc_case = load_data.load_solutions("CaseInput")
case_id_case = case_id
for k in solution_case.keys():
    tmp = ['NA'] * len(case_id_case)
    for i in solution_case[k].keys():
        if i in case_id_case:
            tmp[case_id_case.index(i)] = str(solution_case[k][i])
    solution_case[k] = tmp
csvfile = open("allSubmissions_case.csv","wb")
csvwriter = csv.writer(csvfile)
csvwriter.writerow(lines[0])
for i in range(len(case_id)):
    tmp = [case_id[i]]
    for j in range(1,len(lines[0])):
        tmp.append(solution_case[lines[0][j]][i])
    csvwriter.writerow(tmp)
csvfile.close()

"""

# load all solutions that are case-only
csvfile = open("allSubmissions_case.csv","rb")
csvreader = csv.reader(csvfile)
lines = [line for line in csvreader]

# case id for all patients
case_id_case = []

# each solution is indexed by its name, its value is a list
solution_case = {}
for j in range(1,len(lines[0])):
    solution_case[lines[0][j]] = []

for k in range(1,len(lines)):
    case_id_case.append(lines[k][0])
    for j in range(1,len(lines[k])):
        solution_case[lines[0][j]].append(lines[k][j])
csvfile.close()

# compute the percentage of 'NA' for each sample
perc_na_all = np.zeros(len(case_id))
perc_na_case = np.zeros(len(case_id_case))
for i in range(len(case_id)):
    for k in solution.keys():
        if solution[k][i] == 'NA':
            perc_na_all[i] += 1./len(solution)
        if solution_case[k][i] == 'NA':
            perc_na_case[i] += 1./len(solution_case)
"""
plt.figure(1)
plt.plot(sorted(perc_na_all))
plt.xlabel('sample index')
plt.ylabel("percentage of 'NA' (AllInput)")
plt.figure(2)
plt.plot(sorted(perc_na_case))
plt.xlabel("sample index")
plt.ylabel("percentage of 'NA' (CaseInput)")
plt.show()
"""

# choose which set of input solutions to use for consensus
flag_solution = 'AllInput'
if flag_solution == 'AllInput':
    case_id_tmp = case_id
    solution_tmp = solution
    thd_drop_sample = 0.6
    perc_na_tmp = perc_na_all
elif flag_solution == 'CaseInput':
    case_id_tmp = case_id_case
    solution_tmp = solution_case
    thd_drop_sample = 0.75
    perc_na_tmp = perc_na_case
else:
    pass

# drop samples according to threshold
flag_dropsample = False
if flag_dropsample == True:
    case_id_sel = []
    solution_sel = {}
    for k in solution_tmp.keys():
        solution_sel[k] = []
    for i in range(len(case_id_tmp)):
        if perc_na_tmp[i] < thd_drop_sample:
            case_id_sel.append(case_id_tmp[i])
            for k in solution_sel.keys():
                solution_sel[k].append(solution_tmp[k][i])
else:
    case_id_sel = case_id_tmp
    solution_sel = solution_tmp


# specify output directory
if flag_solution == 'AllInput':
    if flag_dropsample == True:
        dir_sol = "sol_consensus_AllInput_dropsample"
        dir_fig = "fig_consensus_AllInput_dropsample"
    else:
        dir_sol = "sol_consensus_AllInput"
        dir_fig = "fig_consensus_AllInput"
elif flag_solution == "CaseInput":
    if flag_dropsample == True:
        dir_sol = "sol_consensus_CaseInput_dropsample"
        dir_fig = "fig_consensus_CaseInput_dropsample"
    else:
        dir_sol = "sol_consensus_CaseInput"
        dir_fig = "fig_consensus_CaseInput"
else:
    pass

# overload symbols to avoid confusion
solution = solution_sel
case_id = case_id_sel

# generate method groups
sol_group,map_group_desc = ensemble_clust.group_methods("subList_yale.csv")

# extract solutions used for consensus clustering
if flag_solution == "AllInput":
    n_cluster_dict = {'RFLobar_AB':3,'Brian':4,'clin_bm':10,'coMbET_bm':2,\
            'Ctvisual_DAL':4,'TAC1_EAR':2,'prism_ew':3,'traj_jcr':6,\
            'Cntrl_A_JDC':5,'Factor_jh':3,'cc_jxc':4,'sc_jxc':4,'grYel_KK':4,\
            'turquoise_KK':4,'lion_KRG':3,'asthma_LPH':3,'Overlap_mh':2,\
            'prm_mh':4,'urf_pc':4,'thorax_pjc':4,'O2d_rpb':4,'lhTraj_rse':6,\
            'cb_vk':3,'airway_yc':3,'emph_yc':3,'nbs_yc':4}
elif flag_solution == "CaseInput":
    n_cluster_dict = {'RFLobar_AB':2,'Brian':4,'clin_bm':5,'coMbET_bm':2,\
            'Ctvisual_DAL':3,'TAC1_EAR':2,'prism_ew':3,'traj_jcr':5,\
            'Cntrl_A_JDC':4,'Factor_jh':3,'cc_jxc':3,'sc_jxc':4,'grYel_KK':4,\
            'turquoise_KK':3,'lion_KRG':3,'asthma_LPH':3,'Overlap_mh':2,\
            'prm_mh':4,'urf_pc':3,'thorax_pjc':3,'O2d_rpb':3,'lhTraj_rse':4,\
            'cb_vk':2,'airway_yc':2,'emph_yc':2,'nbs_yc':3}
else:
    pass

# generate a consensus clustering solution for each method of one investigator
for group_id_sel in sol_group.keys():
    
    # handle Lystra Hayden's solutions separately
    if group_id_sel == 'asthma_LPH':
        solution_use = {}
        assert set(sol_group[group_id_sel]) == set(['cAsthma_LPH',\
                'cPNAandA_LPH','cPNA_LPH','cPNAorA_LPH'])
        group_1 = {}
        group_2 = {}
        group_3 = {}
        for i in range(len(case_id)):
            if solution['cPNAandA_LPH'][i] == '1':
                group_1[case_id[i]] = '1'
            else:
                if solution['cPNA_LPH'][i] == '1':
                    group_2[case_id[i]] = '2'
                if solution['cAsthma_LPH'][i] == '1':
                    group_3[case_id[i]] = '3'
        assert len(set(group_2).intersection(set(group_3))) == 0
        tmp = group_1.copy()
        tmp.update(group_2)
        tmp.update(group_3)
        solution_use = {group_id_sel:tmp}
    
    # handle Meilan Han's solutions separately
    elif group_id_sel == 'prm_mh':
        solution_use = {}
        assert set(sol_group['prm_mh']) == set(['prm2_mh','prm_mh'])
        group_1 = {}
        group_2 = {}
        for i in range(len(case_id)):
            if solution['prm_mh'][i] in ['0','1']:
                group_1[case_id[i]] = solution['prm_mh'][i]
            if solution['prm2_mh'][i] in ['3','4']:
                group_2[case_id[i]] = solution['prm2_mh'][i]
        assert len(set(group_1).intersection(set(group_2))) == 0
        tmp = group_1.copy()
        tmp.update(group_2)
        solution_use = {group_id_sel:tmp}

    # consensus clustering for other solutions
    else:
        solution_use = {}
        print group_id_sel
        for k in sol_group[group_id_sel]:
            if k in solution.keys():
                solution_use[k] = {}
                # only find the case_ids that have cluster labels
                for i in range(len(case_id)):
                    if solution[k][i] != 'NA':
                        solution_use[k][case_id[i]] = solution[k][i]
            else:
                print k,"is missing in allSubmissions.csv"

    # construct hypergraph
    H,case_id_unique = ensemble_clust.construct_hypergraph(solution_use,True)

    # similarity matrix
    S = 1./len(solution_use) * H.dot(H.T)
    
    n_cluster = n_cluster_dict[group_id_sel]
    # apply spectral clustering
    label_pred = spectral_clustering(S,n_clusters=n_cluster)
    

    # write consensus solution into a csv file
    if not os.path.exists(dir_sol):
        os.makedirs(dir_sol)
    csvfile = open(dir_sol+"/"+group_id_sel,"wb")
    csvwriter = csv.writer(csvfile)
    assert len(label_pred) == len(case_id_unique)
    for i in range(len(case_id_unique)):
        csvwriter.writerow([case_id_unique[i],label_pred[i]])
    csvfile.close()
    # draw similarity matrix
    if not os.path.exists(dir_fig):
        os.makedirs(dir_fig)
    draw_similarity_matrix.draw_similarity_matrix(S,label_pred,n_cluster,\
            dir_fig+"/sim_"+group_id_sel+'.png')
