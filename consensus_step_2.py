""" Based on the output of "consensus_step_2.py", apply consensus clustering on
the those solutions, in the end, we can get a single clustering solution 
"""

import numpy as np
import csv
import os
import string
from utils import load_data
from utils import ensemble_clust
from utils import draw_similarity_matrix
import scipy.cluster.hierarchy as sch
import matplotlib.pyplot as plt
import pylab
from sklearn.cluster import spectral_clustering

flag_solution = "AllInput"
flag_dropsample = False

if flag_solution == 'AllInput':
    if flag_dropsample == True:
        dir_sol = "sol_consensus_AllInput_dropsample"
        dir_fig = "fig_consensus_AllInput_dropsample"
    else:
        dir_sol = "sol_consensus_AllInput"
        dir_fig = "fig_consensus_AllInput"
elif flag_solution == "CaseInput":
    if flag_dropsample == True:
        dir_sol = "sol_consensus_CaseInput_dropsample"
        dir_fig = "fig_consensus_CaseInput_dropsample"
    else:
        dir_sol = "sol_consensus_CaseInput"
        dir_fig = "fig_consensus_CaseInput"
else:
    pass



# load all solutions
solution,feature_of_interest,subtype_name,description = \
        load_data.load_solutions(dir_sol)

# construct hypergraph
H,case_id_unique = ensemble_clust.construct_hypergraph(solution,True)
print "hypergraph computation finished"

# similarity matrix
S = 1./len(solution) * H.dot(H.T)

for n_cluster in range(2,7):
    print n_cluster
    # apply spectral clustering
    label_pred = spectral_clustering(S,n_clusters=n_cluster)
    # draw similarity matrix
    draw_similarity_matrix.draw_similarity_matrix(S,label_pred,n_cluster,\
            dir_fig + "/sim_sol_consensus"+'_K_'+str(n_cluster)+'.png')

    # write consensus solution into a csv file
    if not os.path.exists(dir_sol+"_con"):
        os.makedirs(dir_sol+"_con")
    csvfile = open(dir_sol + "_con" + "/sol_consensus"+'_K_'+str(n_cluster),"wb")
    csvwriter = csv.writer(csvfile)
    assert len(label_pred) == len(case_id_unique)
    for i in range(len(case_id_unique)):
        csvwriter.writerow([case_id_unique[i],label_pred[i]])
    csvfile.close()



