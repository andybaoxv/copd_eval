import numpy as np
import csv

csvfile = open("subList_yale.csv","rb")
csvreader = csv.reader(csvfile)
lines = [line for line in csvreader]

# solution id, unique
solution_id = []

# author id, not unique, one author can have multiple solutions
author_id = []

# description for methods, not unique, one description can correspond to
# multiple solutions
desc_id = []

# group id,
group_id = []

# solution group, dictionary, group_id as key and solution_id as values
sol_group = {}

# map group id to desc_id
map_group_desc = {}

for i in range(1,len(lines)):
    solution_id.append(lines[i][0])
    author_id.append(lines[i][2])
    desc_id.append(lines[i][3])
    group_id.append(lines[i][4])
    map_group_desc[group_id[-1]] = desc_id[-1]

    if group_id[-1] not in sol_group.keys():
        sol_group[group_id[-1]] = [solution_id[-1]]
    else:
        sol_group[group_id[-1]].append(solution_id[-1])

assert len(np.unique(desc_id)) == len(np.unique(group_id))

for item in sol_group.keys():
    print item, sol_group[item]

