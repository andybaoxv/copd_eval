import numpy as np
import csv
import os
import string
from utils import ensemble_clust
from sklearn.cluster import spectral_clustering
from sklearn.metrics import jaccard_similarity_score
from optparse import OptionParser

desc = """ This script is used to compute consensus clustering solution given a
set of solutions
"""
parser = OptionParser(description=desc)
parser.add_option('--fi',\
        help='input file name containing solutions',\
        dest='filename_input',metavar='<filename_input>',\
        default='ensemble_input')
parser.add_option('--fo',\
        help='output file name containing consensus solution',\
        dest='filename_output',metavar='<filename_output>',\
        default='ensemble_output')
parser.add_option('--k',\
        help='number of clusters in consensus solution',\
        dest='n_cluster',nargs=1,metavar='<n_cluster>',type='int',\
        default=4)
parser.add_option('--method',\
        help='method used for computing consensus clustering',\
        dest='flag_method',metavar='<flag_method>',default='MCLA')

parser.add_option('--union',\
        help='whether to take union of case ids of all input solutions',\
        dest='flag_union',metavar='<flag_union>',type='int',\
        default=0)

(options,args) = parser.parse_args()

############################## Parameter Settings ############################
# filename of textfile contaiing clustering solution names, each row has one
# solution
print "Parameter Settings"
print "==================="
filename_input = options.filename_input
print "input file: ", filename_input

# output files containing the consensus solution
filename_output = options.filename_output
print "output file: ", filename_output

# number of clusters in final solution (this value shoud be smaller than the
# total number of clusters in all input solutions for consensus if 'MCLA' is used)
n_cluster = options.n_cluster
print "cluster number in consensus solution: ",n_cluster

# method used for getting the consensus: {'CSPA','MCLA'}
flag_method = options.flag_method
print "consensu method: ", flag_method

# whether to take the union of case-ids in all input solutions
flag_union = options.flag_union
if flag_union == True:
    print "take union of case ids in all input solutions"
else:
    print "take intersection of case ids in all input solutions"
##############################################################################


# load all solutions
csvfile = open("allSubmissions.csv","rb")
csvreader = csv.reader(csvfile)
lines = [line for line in csvreader]

# case id for all patients
case_id = []

# each solution is indexed by its name, its value is a list
solution = {}
for j in range(1,len(lines[0])):
    solution[lines[0][j]] = []

for k in range(1,len(lines)):
    case_id.append(lines[k][0])
    for j in range(1,len(lines[k])):
        solution[lines[0][j]].append(lines[k][j])
csvfile.close()


# choose solution_id used for computing consensus
solution_id_use = {}

# extract solution_id from input text file
assert os.path.isfile(filename_input) == True, "input filename does not exist" 
txtfile = open(filename_input,"rb")
# assume the delimiter between solution id and cluster id is tab
csvreader = csv.reader(txtfile)
lines = [line for line in csvreader]
txtfile.close()
for i in range(len(lines)):
    if len(lines[i]) > 1:
        solution_id_use[lines[i][0]] = lines[i][1:]
    else:
        solution_id_use[lines[i][0]] = []

print "\ninput solutions for consensus: "
print "solution_id | cluster_id"
print "-------------------------"
for key in solution_id_use.keys():
    print key,solution_id_use[key]

# extract solutions from the complete solution dataset
solution_use = {}
for item in solution_id_use.keys():
    if item in solution.keys():
        # use all clusters if there is no clusters specified
        if len(solution_id_use[item]) == 0:
            solution_use[item] = dict(zip(case_id,solution[item]))
        # only select samples in that cluster
        else:
            clust_id = solution_id_use[item]
            case_id_sel = []
            label_sel = []
            for i in range(len(case_id)):
                if solution[item][i] in clust_id:
                    case_id_sel.append(case_id[i])
                    label_sel.append(solution[item][i])
            solution_use[item] = dict(zip(case_id_sel,label_sel))

solution_missing = []
for item in solution_use.keys():
    if item not in solution.keys():
        solution_missing.append(item)
if len(solution_missing) > 0:
    print "\nsolutions not found in the allSubmissions.csv: \n"
    for item in solution_missing:
        print item
else:
    print "\nall input solutions are found in allSubmissions.csv"

# Apply consensus clustering on the input solutions

print "\nbegin consensus clustering..."
print "================================="

# construct hypergraph
H,case_id_unique = ensemble_clust.construct_hypergraph(solution_use,False)
assert H.shape[0] > 0, "there's no overlapping between case ids in all input solutions"

# Method 1: CSPA (Cluster-based Similarity Partitioning Algorithm)
if flag_method == 'CSPA':
    print "computation of hypergraph finished..."
    S = 1./len(solution_use) * H.dot(H.T)
    print "computation of similarity matrix finished..."
    label_consensus = spectral_clustering(S,n_clusters=n_cluster)
    print "computation of spectral clustering finished..."
    # write the clustering solution to output file
    csvfile = open(filename_output,"wb")
    csvwriter = csv.writer(csvfile)
    for i in range(len(label_consensus)):
        csvwriter.writerow([case_id_unique[i],label_consensus[i]])
    csvfile.close() 
    print "consensus solution is stored in: ",filename_output

elif flag_method == 'MCLA':
    print "the number of clusters you specified is required be smaller than"+\
            " the total number of clusters in all the input solutions for consensue"
    print "computation of hypergraph finished..."
    # compute jaccard similarity score between pairwise hypergraph edges
    sim_H_edge = np.zeros((H.shape[1],H.shape[1]))
    for i in range(H.shape[1]):
        for j in range(i,H.shape[1]):
            sim_H_edge[i,j] = jaccard_similarity_score(H[:,i],H[:,j])
            sim_H_edge[j,i] = sim_H_edge[i,j]
    print "construction of metagraph finished..."
    label_H_edge = spectral_clustering(sim_H_edge,n_clusters=n_cluster)
    # group hyper edges according to clustering solution
    group_H_edge = []
    for k in range(n_cluster):
        group_H_edge.append([])
    for i in range(len(label_H_edge)):
        group_H_edge[label_H_edge[i]].append(i)
    # find the mean of each meta cluster of hyperedges
    H_edge_clust_mean = []
    for idx in group_H_edge:
        H_edge_clust_mean.append(np.mean(H[:,idx],axis=1))
    H_edge_clust_mean = np.array(H_edge_clust_mean).T
    # Compete for objects
    label_consensus = []
    for i in range(H_edge_clust_mean.shape[0]):
        tmp_max = np.max(H_edge_clust_mean[i,:])
        label_consensus.append(list(H_edge_clust_mean[i,:]).index(tmp_max))
   
    # write the clustering solution to output file
    csvfile = open(filename_output,"wb")
    csvwriter = csv.writer(csvfile)
    for i in range(len(label_consensus)):
        csvwriter.writerow([case_id_unique[i],label_consensus[i]])
    csvfile.close()
    print "consensus solution is stored in: ",filename_output

else:
    print "The method you choose is not correct!"
