""" This script is used to make scatterplot of different clustering solutions 
"""

import numpy as np
import csv
from utils import load_data
from utils.eval_phenotype import eval_phenotype
from utils import clust_cmp
from optparse import OptionParser
import os
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

############## Step 1: Load Phenotype and Genotype Data #################

# load the original 10,000 COPD phenotype data without imputation
filename_cg_merged = "copd_data/cgSubtypeMerged.csv"
feature_name,case_id_pheno,data_pheno = \
        load_data.load_cg_merged(filename_cg_merged)

# load the original COPD genotype data
#filename_geno = "copd_data/COPDGene_Genotypes.txt"
#geno_feat_name,case_id_geno,data_geno = \
#        load_data.load_data_geno(filename_geno)

############## Step 2: Load Clustering Solutions ###########################

# directory containing multiple clustering solutions
# dir_name = "Results_RF_PJC"
dir_name = "scatter_input"
solution,feature_of_interest,subtype_name,description = \
        load_data.load_solutions(dir_name)

solution_id = solution.keys()
idx_sel = []
label = []
for k in range(len(case_id_pheno)):
    if case_id_pheno[k] in solution[solution_id[0]].keys():
        idx_sel.append(k)
        label.append(solution[solution_id[0]][case_id_pheno[k]])

# extract features from the original dataset
feat_axis_name = ['pctEmph_Slicer','WallAreaPct_seg','Exacerbation_Frequency']
feat_axis_val = []
for k in feat_axis_name:
    if k in feature_name:
        feat_axis_val.append(data_pheno[idx_sel,feature_name.index(k)])

feat_axis_val = np.array(feat_axis_val).T

n_instances = feat_axis_val.shape[0]
for i in range(n_instances):
    for j in range(feat_axis_val.shape[1]):
        if feat_axis_val[i,j] == 'NA':
            feat_axis_val[i,j] = float('nan')
        else:
            feat_axis_val[i,j] = float(feat_axis_val[i,j])

color = ['b','r','g','m','y','k']
fig = plt.figure()
feat_x = list(feat_axis_val[:,0])
feat_y = list(feat_axis_val[:,1])
feat_z = list(feat_axis_val[:,2])

flag_3d = False

if flag_3d == True:
    ax = fig.add_subplot(111,projection='3d')
    for i in range(n_instances):
        ax.scatter(feat_x[i],feat_y[i],feat_z[i],c=color[label[i]])
    ax.set_xlabel(feat_axis_name[0])
    ax.set_ylabel(feat_axis_name[1])
    ax.set_zlabel(feat_axis_name[2])

# 2d plot
else:
    for i in range(n_instances):
        plt.scatter(feat_x[i],feat_y[i],c=color[label[i]])
    plt.xlabel(feat_axis_name[0])
    plt.ylabel(feat_axis_name[1])

plt.show()

