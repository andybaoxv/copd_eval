The evaluation pipeline is implemented in Python. The following packages
are necessary to run the script:
1) Numpy
2) Scipy
3) Matplotlib
4) Scikit-learn

After changing directory to ~/copd_eval/
Run script through command line in the following form
====================================================================
python main_copd_eval.py --di input_directory --do output_directory
====================================================================

--di is the flag for input directory that contains multiple clustering
solutions;
--do is the flag for output directory that stores clustering analsysi results;

Also note that the input phenotype feature list is assumed to be stored in file
"feat_pheno_use_infoV2.csv", 
where each row stores one phenotype feature of interest.

An illustrating example is:

python main_copd_eval.py --di Results_RF_PJC --do output_eval_pheno

If you use iPython, the following command can be used instead

run main_copd_eval.py --di Results_RF_PJC --do output_eval_pheno

========================== Output ==============================

1) output_directory
directory storing the analysis results for each solution specified in
input_directory.
    a) For binary features, the percentages of 1's are used as output;
       For continuous features, median (IQR) are used as output;
       In particular, for SNPs, mean (std) are used as output.
    b) p-value for each feature is computed:
        Kruskal_Wallis Test is applied for continuous features;
        Chi-square Test is applied for binary features;
    c) Percentage of missing values for each feature is also computed

overall comparison between input solutions is also computed and the results are
stored under directory "output_directory/fig"
    a) NMI_1: for pairwise solutions, consider the intersection of samples and
    apply NMI
    
    b) NMI_2: for pariwise solutions, consider the union of samples, assign
    samples appearing only in one solution to a separate cluster with ID -10
    
    c) concordance: for pariwise solution, compute the similarity between their
    clusters using concordance score, i.e. the ratio between the number of
    samples in intersection and the number of samples in union, take the
    maximum concordance score between clusters as the concordance score between
    these two solutions.
    
    d) sim_clust_1: compute the similarity between all the clusters in all input
    solution using concordance score, as is suggested by Jennifer. Similar to
    the difference between NMI_1 and NMI_2, here we have sim_clust_1 for the
    case of finding overlap between solutions before computing concordance
    score between clusters.
    
    e) sim_clust_2: compute concordance score between clusters directly without
    finding overlap between samples in two solutions.
     
    e) solution by feature p-value heatmap


