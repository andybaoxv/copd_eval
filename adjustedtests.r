## read in the transformed data.
df<-read.csv("copd_data/varTransformed.csv")
allS<-read.csv("allSubmissions.csv")
df<-merge(df,allS,by="sid")
## standard adjustments
adj<-c("nhw","male","Age_Enroll","ATS_PackYears","SmokCigNow")
adj2<-c("FEV1pp_utah","FEV1_FVC_utah")
## for biomarkers, need to actually do LLQ?
feats<-read.csv("feat_pheno_use_infoV2.csv")
### initialize the data frame
res<-data.frame(sols=names(allS)[-1])
res<-cbind(res,matrix(nrow=nrow(res),ncol=nrow(feats)))
names(res)[-1]<-feats$Feature.Name
for (sType in names(allS)[-1]) {
    ## first set of features - nonSNP related
    for (i in 1:nrow(feats)) {
        if (feats$Class[i]=="Demographic") next ## here the kruskal wallis is fine - we don't want any adjustments here
        ## ## first section - find the proper feature name
        ## ## for the SNPs, rounded to 0,1,2 for ordered logistic regression
        ## ## for the continuous variables, just the variable name followed by "Rn"
        ## ## binary variables are not transformed
        
        # Yale: y is the new feature name
        # SNPs features exluding the four risk scores constructed by Brian and Ed
        if (feats$Class[i]=="SNPs" & !grepl("Score",feats$Feature.Name[i])) {
            y<-paste0(feats$Feature.Name[i],"012")
        } else if (feats$Type[i]=="continuous" & !grepl("Score",feats$Feature.Name[i])) {
            y<-paste0(feats$Feature.Name[i],"Rn")
        } else {
            y<-feats$Feature.Name[i]
        }
        print(y) ## debugging purposes
        ## y now holds the outcome variable, build the formula -- we adjust for all the standard variables
        form1<-paste0(y,"~",paste(adj,collapse="+"))
        ## and, unless we are looking at lung function, adjust for baseline degree of obstruction
        if (!feats$Feature.Name[i] %in% c("FEV1pp_utah","FEV1_FVC_utah")) form1<-paste0(form1,"+",paste(adj2,collapse="+"))
        form2<-paste0(form1,"+as.factor(",sType,")")
        df2<-df[complete.cases(df[,names(df) %in% c(adj,adj2,y,sType)]),]
        if (length(table(df2[,sType]))==1) next
        # linear model for continuous features + risk scores in SNPs features
        if (feats$Type[i]=="continuous" & (feats$Class[i]!="SNPs" | grepl("Score",feats$Feature.Name[i]))) {
            mod1<-lm(as.formula(form1),data=df2)
            mod2<-lm(as.formula(form2),data=df2)
        # general linear model for binary features
        } else if (feats$Type[i]=="binary") {
            ## is binary - no other types
            mod1<-glm(as.formula(form1),data=df2,family=binomial)
            mod2<-glm(as.formula(form2),data=df2,family=binomial)
        } else {
            ## it is something genetic... need different formulas
            form1<-paste0("as.factor(",y,")~Age_Enroll+ATS_PackYears+",paste0("pc",1:5,".nhw",collapse="+"))
            form2<-paste0(form1,"+as.factor(",sType,")")
            ## look at cell counts - if no homozygous (or too few homozygous) need to collapse
            tab<-table(df2[,y])
            if (length(tab)==2 | min(tab) < 5) {
                if (min(tab) < 5) df2[df2[,y]==names(which.min(tab)),y]<-1
                mod1<-glm(as.formula(form1),data=df2,family=binomial)
                mod2<-glm(as.formula(form2),data=df2,family=binomial)
            } else {
                mod1<-polr(as.formula(form1),data=df2)
                mod2<-polr(as.formula(form2),data=df2)
            }
        }
        ## do the likelihood ratio test
        res[res$sols==sType,feats$Feature.Name[i]]<-lmtest::lrtest.default(mod1,mod2)$"Pr(>Chisq)"[2]
    }
}
## write.csv(res,"/proj/regeps/regep00/studies/COPDGene/analyses/Remhc/Subtype/adjustedOutcomes.csv",row.names=F)
